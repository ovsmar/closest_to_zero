import re

while True:
    data = input("Temperature: ")
    x = data.split(" ")
    if re.match("-?\d* ?", x[0]).span() != (0,0):
        break
    print("erreur d'input")

to_int = [int(numeric_string) for numeric_string in x]
min = to_int[0]
for element in to_int:
    if (abs(element) < abs(min)):
        min = element
    elif abs(element) == abs(min):
        min = max(element,min) 
        
print(min)
    